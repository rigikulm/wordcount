# wordcount
“Prints the total wordcount and the number of times each word occurs”

    This code is based on the similarly named programs from the book
    "The Go Programming Language" by Alan Donovan, and Brian Kernighan.

## Sample Programs

**wordcount usage**

``` bash
# Displays the word count
$ wordcount < foo.txt
  42 words
$
# Displays detailed wordcount information
$ wordcount -l < foo.txt
  42 words
  17 the
  16 a
   9 is
$
```
